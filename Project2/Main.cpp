

#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include "resource.h"
#include "Main.h"

HINSTANCE hInst;
HBITMAP hBMP = 0;
HWND hBitmap = 0, hButton = 0;
BOOL hidden = FALSE;

BOOL CALLBACK DialogProc(HWND H,UINT M,WPARAM W,LPARAM L)
{
    switch(M)
    {
        case WM_INITDIALOG:
        {
          hButton = GetDlgItem(H,BTN_TEST);
          hBitmap = GetDlgItem(H,STC_BMP);
          hBMP = LoadBitmap(hInst,MAKEINTRESOURCE(BMP_TEST));
          SendMessage(hBitmap,STM_SETIMAGE,(WPARAM)IMAGE_BITMAP,(LPARAM)hBMP);
          ShowWindow(hBitmap,SW_SHOW);
          return TRUE;
        }
        case WM_CLOSE:
        {
          DeleteObject(hBMP);
          EndDialog(H,0);
          return TRUE;
        }
        case WM_COMMAND:
        {
           switch(LOWORD(W))
           {
             case BTN_TEST:
             {
				 EndDialog(H, 0);
                /*if(hidden)
                {
                  hidden = FALSE;
                  ShowWindow(hBitmap,SW_SHOW);
                  SendMessage(hButton,WM_SETTEXT,0,(LPARAM)"Cancel");
                  return TRUE;
                } else {
                  hidden = TRUE;
                  ShowWindow(hBitmap,SW_HIDE);
                  SendMessage(hButton,WM_SETTEXT,0,(LPARAM)"Cancel");
                  return TRUE;
                } */
             }
          }
       }
    }
    return FALSE;
}


int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd)
{
    hInst = hInstance;
    return DialogBox(hInstance, MAKEINTRESOURCE(DLG_MAIN), NULL, (DLGPROC)DialogProc);
}
